# Aide et ressources de RefFIT pour Synchrotron SOLEIL

[<img src="https://reffit.ch/wp-content/uploads/2018/11/logo_150px.png" width="100"/>](https://reffit.ch/software/)

## Résumé

- extraire les constantes optiques à partir des mesures infrarouges
- Privé

## Sources

- Code source:  https://reffit.ch/software/
- Documentation officielle:  https://reffit.ch/wp-content/uploads/2018/10/Manual.pdf

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriaux officiels](https://reffit.ch/wp-content/uploads/2018/10/Tutorial.zip) |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: tableaux (texte)
- en sortie: tableaux (texte)
- sur un disque dur
